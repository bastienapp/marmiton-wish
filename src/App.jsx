import Recipe from "./components/Recipe";
import TitleH3 from "./components/TitleH3";
import "./App.css";

function App() {
  // pourquoi des parenthèses dans le return ?
  // -> pour faire un return sur plusieurs lignes

  // c'est quoi <> et </>
  // pourquoi mettre des div ?
  // quoi faire à la place ?
  // -> en JSX, il faut que mon retour est un élément parent principal
  //   - soit par exemple une balise (ex: div, p, section, article...)
  //   - soit un fragment : <> et fermer avec </>

  const foodList = [
    {
      id: 1,
      name: "Tacos végétarien",
      rate: 1.3,
      picture: "https://assets.afcdn.com/recipe/20190212/87658_w600.jpg",
      reviews: 12,
      author: {
        firstName: "Michel",
        lastName: "Durand"
      }
    },
    {
      id: 2,
      name: "Lasagnes",
      rate: 5.0,
      picture: "https://assets.afcdn.com/recipe/20200408/109520_w600.jpg",
      reviews: 57,
    },
    {
      id: 3,
      name: "Tartiflette",
      rate: 6,
      picture: "https://assets.afcdn.com/recipe/20160401/38946_w600.jpg",
      reviews: 0,
    },
  ];

  return (
    <>
      <h1 className="title">Marmiton Wish</h1>
      <TitleH3 title="La version plus naze" color="blue" />

      <section className="recipe__container">
        {foodList.map((eachFood) => (
          <Recipe
            key={eachFood.id}
            name={eachFood.name}
            reviews={eachFood.reviews}
            picture={eachFood.picture}
            rate={eachFood.rate}
            author={eachFood.author}
            /*{...eachFood}*/
          />
        ))}
      </section>
    </>
  );
}

export default App;
