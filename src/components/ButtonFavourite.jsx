function ButtonFavourite(props) {
  // props drilling, state up lifting
  /*
  props: {
    favourite,
    changeFavourite
  }
  */

  return <button onClick={() => props.changeFavourite(!props.favourite)}>
    {
      props.favourite ? "Retirer des favoris" : "Ajouter aux favoirs"
    }
  </button>;
}

export default ButtonFavourite;
