import { useState } from "react"; // hook
import "./Recipe.css";
import TitleH3 from "./TitleH3";

//  Recipe({ picture, name, rate, reviews })
function Recipe(props) {
  // décomposition
  const { picture, name, rate, reviews, author } = props;
  // ici c'est du JavaScript "pur"
  /*
  props: {
    name: "Nom de la recette",
    rate: 3.14,
    picture: "...",
    reviews: 0
  }
  */
  // opérateur ternaire : on peut l'utiliser dans le JSX
  // condition ? valeur si vrai : valeur si faux
  reviews > 0 ? reviews + " avis" : "Aucun avis";

  //     valeur       fonction pour          état de départ
  //                modifier la valeur      de la valeur
  //                rafraîchit l'affichage
  //
  /*
    useState : ça retourne un tableau avec deux cases
      - 1 : la valeur
      - 2 : la fonction pour modifier la valeur
  */
  // const newState = useState(false);
  // const isFavourite = newState[0];
  // const setIsFavourite = newState[1];
  const [isFavourite, setIsFavourite] = useState(false);
  // décomposition de tableau
  // const [fruit, légume] = ["Ananas", "Courgette"];

  // comment on pourrait faire sans déclarer addToFavourite
  function toggleFavourite() {
    // modifi(e) la valeur du state : vrai
    if (isFavourite) {
      setIsFavourite(false);
    } else {
      setIsFavourite(true);
    }
  }

  // le JSX est dans le return
  return (
    <article className="Recipe">
      <TitleH3 title={name} />
      <img src={picture} alt={name} />
      <div>{rate} / 5</div>
      <div>{reviews > 0 ? reviews + " avis" : "Aucun avis"}</div>
      {/* <button type="button" onClick={() => setIsFavourite(!isFavourite)}> */}
      {
        (author != null)
          ? <p>{author.firstName + " " + author.lastName}</p>
          : "Aucun auteur"
      }
      <button type="button" onClick={toggleFavourite}>
        {isFavourite ? "Recette favorite" : "Ajouter ma recette aux favoris"}
      </button>
    </article>
  );
}
// pour rendre accessible mon composant au reste de l'application
export default Recipe;
