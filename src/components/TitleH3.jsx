// props : les paramètres reçus par la fonction : c'est un objet, qui contient des propriétés
  // function TitleH3({ title, name }) {
  function TitleH3(props) {
  /*
  props: {
    title: "La version plus naze",
    color: "blue"
  }
  */

  // pour faire du JavaScript dans du JSX, on doit ajouter des accolades
  return <h3 style={{ color: props.color }}>{props.title}</h3>;
}

export default TitleH3;
